package beertag.MockObjects;


import beertag.DTOs.UserDto;
import beertag.models.*;
import beertag.repositories.repositoryContracts.BeerRatingRepository;
import beertag.repositories.repositoryContracts.BeersRepository;
import beertag.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.verification.NeverWantedButInvoked;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import beertag.repositories.repositoryContracts.UsersRepository;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class UserServiceImplTests {


    @Mock
    private UsersRepository userMockRepository;
    private BeersRepository beersRepository;
    private BeerRatingRepository beerRatingRepository;


    @InjectMocks
    private UserServiceImpl service;


    // private String name;
    //    private Country country;
    //    private double abv;
    //    private Style style;
    //    private String picture;
    ///////////////// HELP

    private Beer tempBeer() {

        User user = new User("userName1", "password1", "firstName1", "lastNamed2", "picture1");

        Country country = new Country("BG", "Bulgaria");
        Style style = new Style("Pilsener");

        return new Beer("Amstel", country, 2.5, style, user, "picture");
    }

    private User tempUser() {

        User user = new User("userName1", "password1", "firstName1", "lastNamed2", "picture1");

        return user;
    }


    @Test
    public void getAllUsers_Should_ReturnMatchingUser_WhenMachExist() {

        List<User> list = new ArrayList<>();
        list.add(new User("userName1", "password1", "firstName1", "lastNamed2", "picture1"));
        list.add(new User("userName2", "password2", "firstName1", "lastNamed2", "picture2"));

        when(userMockRepository.getAll())
                .thenReturn(list);

        //Act
        List<User> result = service.getAll();

        //Assert
        Assert.assertEquals(2, result.size());

    }

    @Test
    public void getByUsername_Should_ReturnSameUser_WhenUserExist() {

        when(userMockRepository.getByUsername("username1"))
                .thenReturn(
                        new User("username1", "password1", "firstName1", "lastName1", "picture1"));

        //Act
        User result = service.getByUsername("username1");

        //Assert
        Assert.assertEquals("username1", result.getUsername());

    }

    @Test
    public void create_User_Should_AddToRepo() {

        User user = tempUser();

        service.create(user);
        //Assert
        Mockito.verify(userMockRepository, Mockito.times(1)).create(user);

    }


    @Test
    public void Edit_User_Info() {

        User userToEdit = new User("userNameOld", "passwordOld", "firstNameOld", "lastNamedOld", "pictureOld");

        userToEdit.setUsername("userNameNEW");
        userToEdit.setUserPicture("NewPicture");
        userToEdit.setFirstName("NewFirstName");

        when(userMockRepository.update(userToEdit))
                .thenReturn(
                        new User("userNameNEW", "passwordOld", "firstNameOld", "lastNamedOld", "pictureOld")
                );

        when(userMockRepository.getByUsername("userNameNEW"))
                .thenReturn(
                        userToEdit
                );

        //Act
        List<User> result = Collections.singletonList(service.getByUsername("userNameNEW"));

        //Assert
        Assert.assertEquals("userNameNEW", result.get(0).getUsername());
    }


    @Test
    public void getByUsername_Should_returnUser_IfExists() {

        when(userMockRepository.getByUsername("username1"))
                .thenReturn(
                        new User("username1", "password1", "firstName1", "lastName1", "picture1"));

        //Act
        User result = service.getByUsername("username1");

        //Assert
        Assert.assertEquals("username1", result.getUsername());

    }


    @Test(expected = IllegalArgumentException.class)
    public void getById_Should_ThrowException_When_UserNotExist() {

        // Arrange
        User user = tempUser();
        user.setUserId(1);

        when(userMockRepository.getById(2))
                .thenReturn(null);

        //  Act
        //  User result = service.getById(2);
        // Assert
        service.getById(2);
    }

    @Test(expected = NeverWantedButInvoked.class)
    public void create_Should_ThrowException_When_UsernameIsNotUnique() {
        // Arrange
        User user = new User("SAME_NAME", "password1", "firstName1", "lastNamed2", "picture1");
        Mockito.when(userMockRepository.getAll())
                .thenReturn(Arrays.asList(
                        new User("userName1", "password1", "firstName1", "lastNamed2", "picture1"),
                        new User("SAME_NAME", "password1", "firstName1", "lastNamed2", "picture1"),
                        new User("userName3", "password1", "firstName1", "lastNamed2", "picture1")
                ));

        // Act
        service = new UserServiceImpl(userMockRepository, beersRepository, beerRatingRepository);
        service.create(user);

        // Assert
        Mockito.verify(userMockRepository, Mockito.never()).create(user);
    }
}