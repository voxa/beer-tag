package beertag.controllers;


import beertag.models.Tag;
import beertag.services.servicesContracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/tags")
public class TagControllerRest {


    private TagService tagService;

    @Autowired
    public TagControllerRest(TagService tagService) {
        this.tagService = tagService;
    }


  @GetMapping
    public List<Tag> getAll() {
        return tagService.getAll();
    }

}
