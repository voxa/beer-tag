package beertag.controllers;

import beertag.models.Beer;
import beertag.models.User;

import beertag.services.servicesContracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/api/users")
public class UserControllerRest {


    private UserService userService;


    @Autowired
    public UserControllerRest(UserService userService) {
        this.userService = userService;
    }



    @GetMapping("/me")
    public User getCurrentUser() throws UsernameNotFoundException {
        return userService.getCurrentUser();
    }

    @GetMapping("/wish")
    public List<Beer> getUserWishBeers() {

        try {
            return userService.getUserWishBeers();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }

    }

    @GetMapping("/drank")
    public List<Beer> getUserDrankBeers() {

        try {
            return userService.getUserDrankBeers();
        } catch (IllegalArgumentException ex) {
                   throw new ResponseStatusException(HttpStatus.NOT_FOUND,ex.getMessage());
        }

    }

    @GetMapping("/created")
    public List<Beer> getUserCreatedBeers() {

        try {
            return userService.getUserCreatedBeers();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }

    }


    @PutMapping("/edit")
    public User updateUser(@RequestBody User user) {
        return userService.edit(user);

    }

//    @GetMapping("/{id}")
//    public User getById(@PathVariable int id) {
//
//        try {
//            return userService.getById(id);
//        } catch (IllegalArgumentException ex) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
//        }
//    }


    @GetMapping("/top3")
    public List<Beer> getTop3RankedBeers() {

        try {
            return userService.getTop3RankedBeers();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }


    @PutMapping("/wish/{beerId}")
    public String markUnmarkAsWish(@PathVariable int beerId) {
        return userService.markUnmarkAsWish(beerId);
    }

    @PutMapping("/drank")
    public String markUnmarkAsDrank(@RequestParam(value = "beerId") int beerId) {
        return userService.markUnmarkAsDrank(beerId);
    }

    @PostMapping("/rate")
    public String rateBeer(@RequestParam(value = "beerId") int beerId,
                           @RequestParam(value = "vote") int vote) {
        return userService.rateBeer(beerId, vote);
    }

    @GetMapping("/vote")
    public int getUserRatingForBeer(@RequestParam(value = "beerId") int beerId) {

        try {
            return userService.getUserRatingForBeer(beerId);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
            }


    @DeleteMapping
    public void removeUser() {
        try {
            userService.remove();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }



}
