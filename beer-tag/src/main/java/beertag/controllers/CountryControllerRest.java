package beertag.controllers;

import beertag.models.Country;
import beertag.services.servicesContracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/api/countries")
public class CountryControllerRest {


    private CountryService countryService;

    @Autowired
    public CountryControllerRest(CountryService countryService) {
        this.countryService = countryService;
    }


    @GetMapping
    public List<Country> getAll() {
        return countryService.getAll();
    }

}
