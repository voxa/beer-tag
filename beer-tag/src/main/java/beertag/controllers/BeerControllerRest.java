package beertag.controllers;


import beertag.DTOs.BeerDTO;
import beertag.models.Beer;
import beertag.models.Tag;
import beertag.services.servicesContracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/beers")
public class BeerControllerRest {


    private BeerService beerService;
    //  private BeerRatingService beerRatingService;


    @Autowired
    public BeerControllerRest(BeerService beerService) {
        this.beerService = beerService;

    }

   @PostMapping("/new")
    public Beer createBeer( @RequestBody BeerDTO beer) {
        try {
            return beerService.create(beer);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public String deleteBeer(@PathVariable int id) {
        try {
            return beerService.remove(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

   @GetMapping
    public List<Beer> getAllBeers() {
        return beerService.getAll();
    }

  @GetMapping("/getByName")
    public Beer getBeerByName(@Valid @RequestParam String name) {

        try {
            return beerService.getByName(name);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

 @GetMapping("/update/{id}")
    public Beer updateBeer(@Valid @RequestBody Beer beer, @PathVariable int id) {
     try {
         return beerService.update(id,beer);
     } catch (IllegalArgumentException ex) {
         throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
     }
    }

   @GetMapping("/{id}")
    public Beer getBeerById(@PathVariable int id) {

        try {
            return beerService.getById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

 @GetMapping("/sortByName")
    public List<Beer> sortBeersByName() {
        try {
            return beerService.sortBeersByName();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

   @GetMapping("/sortByAbv")
    public List<Beer> sortBeersByAbv() {
        try {
            return beerService.sortBeersByAbv();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @GetMapping("/sortByRating")
    public List<Beer> sortBeersByRating() {
        try {
            return beerService.sortBeersByRating();
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

   @GetMapping("/getByStyle/{styleId}")
    public List<Beer> getBeersByStyle(@PathVariable int styleId) {
        try {
            return beerService.getBeersByStyle(styleId);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @GetMapping("/getByCountry/{countryId}")
    public List<Beer> getBeersByCountry(@PathVariable int countryId) {
        try {
            return beerService.getBeersByCountry(countryId);
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

   @GetMapping("/getByTag/{tags}")
    public Set<Beer> filterBeersByTag(@PathVariable List<String> tags) {
        try {
            return beerService.filterBeersByTag(tags);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

   @PutMapping("/addTag")
    public String tagBeer(@Valid @RequestBody Tag tag, @RequestParam int beerId) {
       return beerService.addTag(tag, beerId);

    }
}
