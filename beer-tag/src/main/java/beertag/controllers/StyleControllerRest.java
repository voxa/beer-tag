package beertag.controllers;


import beertag.models.Style;
import beertag.services.servicesContracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/api/styles")
public class StyleControllerRest {


    private StyleService styleService;

    @Autowired
    public StyleControllerRest(StyleService styleService) {
        this.styleService = styleService;
    }


 @GetMapping
    public List<Style> getAll() {
        return styleService.getAll();
    }

}
