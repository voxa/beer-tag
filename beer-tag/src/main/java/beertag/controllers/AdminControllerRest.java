package beertag.controllers;


import beertag.models.Beer;
import beertag.models.User;
import beertag.services.servicesContracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/admin")
public class AdminControllerRest {


    private UserService userService;

    @Autowired
    public AdminControllerRest(UserService userService) {
        this.userService = userService;
    }


    @PutMapping("/edit/{id}")
    public User updateUser(@Valid @RequestBody User user, @PathVariable int id) {

        try {
            return userService.edit(user);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }


   @GetMapping
    public List<User> getAllUsers() {
        return userService.getAll();
    }

   @GetMapping("/{id}")
    public User getById(@PathVariable int id) {

        try {
            return userService.getById(id);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping
    public void deleteUser(@RequestParam int userId){
        try{
            userService.deleteUser(userId);
        }
        catch (IllegalArgumentException ex)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    //makeUserAdmin


}
