package beertag.controllers;


import beertag.models.User;
import beertag.DTOs.UserDto;

import beertag.repositories.repositoryContracts.UsersRepository;
import beertag.security.AuthenticationService;
import beertag.services.servicesContracts.UserDtoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.security.Principal;



@RestController

@RequestMapping("/api")
public class HomeController {



    private final UserDtoService userDtoService;
    private final UsersRepository usersRepository;
    private final AuthenticationService authenticationService;

    @Autowired
    public HomeController(UserDtoService userDtoService,
                          UsersRepository usersRepository,
                          AuthenticationService authenticationService) {
        this.userDtoService = userDtoService;
        this.usersRepository = usersRepository;
        this.authenticationService = authenticationService;
    }


    @PostMapping
    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public User save(@Valid @RequestBody UserDto user) {
        try{
        return userDtoService.save(user);
        }
        catch(IllegalArgumentException ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,ex.getMessage());
        }
    }


//    @GetMapping("/login")
//    public String showLogin() {
//        return "login";
//    }
//
//    @GetMapping("/")
//    public String showHomePage(Principal principal) {
//
//        System.out.println(principal.getName());
//
//        return "index";
//    }

}
