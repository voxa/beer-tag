package beertag.services;
import beertag.models.Country;
import beertag.models.Style;
import beertag.models.User;
import beertag.repositories.repositoryContracts.CountryRepository;
import beertag.repositories.repositoryContracts.StyleRepository;
import beertag.services.servicesContracts.CountryService;
import beertag.services.servicesContracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {

    private StyleRepository repository;

    @Autowired
    public StyleServiceImpl(StyleRepository repository) {
        this.repository = repository;
    }


//    @Override
//    public Style getById(int id) {
//
//        Style style = repository.getById(id);
//        if (style== null) {
//            throw new IllegalArgumentException(
//                    String.format("Style with id %d do not exist !", id));
//        }
//        return style;
//    }


    @Override
    public List<Style> getAll() {
        return repository.getAll();
    }

    @Override
    public Style getByName(String name){return repository.getByName(name);}
}
