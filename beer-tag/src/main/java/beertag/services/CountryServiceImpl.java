package beertag.services;

import beertag.models.Country;
import beertag.models.User;
import beertag.repositories.repositoryContracts.CountryRepository;
import beertag.services.servicesContracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private CountryRepository repository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Country> getAll() {
        return repository.getAll();
    }

    @Override
    public Country getByName(String name){ return repository.getByName(name);}
}
