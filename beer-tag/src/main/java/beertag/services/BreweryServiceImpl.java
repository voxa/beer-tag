package beertag.services;


import beertag.models.Brewery;
import beertag.repositories.repositoryContracts.BreweryRepository;
import beertag.services.servicesContracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BreweryServiceImpl implements BreweryService {

    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }


    @Override
    public Brewery getById(int id) {
        return breweryRepository.getById(id);
    }

    @Override
    public Brewery getByName(String name) {
        return breweryRepository.getByName(name);
    }
}
