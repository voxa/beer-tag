package beertag.services;

import beertag.DTOs.BeerDTO;
import beertag.models.Beer;
import beertag.models.Tag;


import beertag.models.User;
import beertag.repositories.repositoryContracts.BeersRepository;
import beertag.repositories.repositoryContracts.TagsRepository;
import beertag.repositories.repositoryContracts.UsersRepository;
import beertag.services.servicesContracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;



import static beertag.Constants.*;


@Service
public class BeerServiceImpl implements BeerService {

    private BeersRepository beersRepository;
    private TagsRepository tagRepository;
    private UsersRepository usersRepository;


    @Autowired
    public BeerServiceImpl(BeersRepository beersRepository, TagsRepository tagRepository , UsersRepository usersRepository) {
        this.beersRepository = beersRepository;
        this.tagRepository = tagRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public List<Beer> getAll() {
        return beersRepository.getAll();
    }

    @Override
    public Beer getByName(String name) {
        return beersRepository.getByName(name);
    }

    @Override
    public Beer create(BeerDTO beerDTO) {

        String name = beerDTO.getName();

        Beer existingBeer = beersRepository.getByName(name);

        if (existingBeer != null) {
            throw new IllegalArgumentException(
                    String.format(BEER_WITH_NAME_ALREADY_EXISTS, name));
        }

        Beer beer = new Beer();

        beer.setName(beerDTO.getName());
        beer.setCountry(beerDTO.getCountry());
        beer.setStyle(beerDTO.getStyle());
        beer.setAbv(0.00);
        beer.setBeerPicture(beerDTO.getPicture());
        beer.setVotes(0);
        beer.setAvgRating(0.0);

        User user = usersRepository.getCurrentUser();
        if(user==null){
            throw new IllegalArgumentException(
                   YOU_HAVE_TO_REGISTER_FIRST);
        }
        beer.setCreator(user);

                    beersRepository.create(beer);

            return beer;
    }

    @Override
    public Beer getById(int beerId) {
        Beer beer = beersRepository.getById(beerId);
        if (beer == null) {
            throw new IllegalArgumentException(String.format(BEER_WITH_ID_DOESN_T_EXIST, beerId));
        }
        return beersRepository.getById(beerId);

    }


    @Override
    public String remove(int beerId) {
        Beer beer = beersRepository.getById(beerId);
        if (beer == null) {
            throw new IllegalArgumentException(
                    String.format(BEER_WITH_ID_DOESN_T_EXIST, beerId));
        }

        beersRepository.remove(beerId);
        return SUCCESSFULLY_REMOVED;

    }

    @Override
    public Beer update(int id,Beer beer) {
          Beer beerToUpdate = getById(id);
        beerToUpdate.setName(beer.getName());
        beerToUpdate.setCountry(beer.getCountry());
        beerToUpdate.setBrewery(beer.getBrewery());
        beerToUpdate.setStyle(beer.getStyle());
        beerToUpdate.setAbv(beer.getAbv());
        beerToUpdate.setDescription(beer.getDescription());
        beerToUpdate.setBeerPicture(beer.getBeerPicture());
        return beersRepository.update(beerToUpdate);
    }

    @Override
    public List<Beer> getBeersByStyle(int styleId) {

        return beersRepository.getBeersByStyle(styleId);
    }

    @Override
    public List<Beer> getBeersByCountry(int countryId) {

        return beersRepository.getBeersByCountry(countryId);
    }

    @Override
    public Set<Beer> filterBeersByTag(List<String> tags) {

        return beersRepository.filterBeersByTag(tags);
    }

    @Override
    public List<Beer> sortBeersByName() {

        return beersRepository.sortBeersByName();
    }

    @Override
    public List<Beer> sortBeersByAbv() {

        return beersRepository.sortBeersByAbv();
    }

    @Override
    public List<Beer> sortBeersByRating() {

        return beersRepository.sortBeersByRating();
    }

    @Override

    public String addTag(Tag tag, int beerID) {
        String name = tag.getName();
        List<Tag> TagAlreadyExist =
                tagRepository
                        .getAll()
                        .stream()
                        .filter(u -> u.getName().equals(name))
                        .collect(Collectors.toList());

        if (!TagAlreadyExist.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format(TAG_WITH_NAME_ALREADY_EXISTS, name));
        }
        tagRepository.create(tag);
        Beer beer = getById(beerID);
        beer.addTag(tag);
        beersRepository.update(beer);
        return TAG_CREATED_SUCCESSFULLY;
    }

//    private void mapBeerDTOtoBeer(BeerDTO dto,Beer beer) {
//        dto.setBeerName(beer.getName());
//        dto.setCountry(beer.getCountry().getName());
//        dto.setAbv(beer.getAbv());
//        dto.setStyle(beer.getStyle().getName());
//        dto.setCreator(beer.getCreator().getUsername());
//
//    }

}
