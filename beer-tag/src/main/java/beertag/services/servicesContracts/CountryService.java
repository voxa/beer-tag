package beertag.services.servicesContracts;

import beertag.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getByName(String name);

}
