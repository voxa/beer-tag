package beertag.services.servicesContracts;

import beertag.DTOs.BeerDTO;
import beertag.models.Beer;
import beertag.models.Country;
import beertag.models.Tag;

import java.util.List;
import java.util.Set;

public interface BeerService {


  //  Beer create(Beer beer);


    String remove(int beerId);

    List<Beer> getAll();

    Beer create(BeerDTO beerDTO);

    Beer getById(int beerId);

    Beer getByName(String name);

    Beer update(int id, Beer beer);

// ~~~~~~

    List<Beer> getBeersByStyle(int styleId);

    List<Beer> getBeersByCountry(int countryId);

    Set<Beer> filterBeersByTag(List<String> tags);

    List<Beer> sortBeersByName();

    List<Beer> sortBeersByAbv();

    List<Beer> sortBeersByRating();

    String addTag(Tag tag, int beerID);

}
