package beertag.services.servicesContracts;

import beertag.models.Tag;

import java.util.List;

public interface TagService {


    void create(Tag tag);

    void remove(int id);

    Tag edit(Tag tag);

    List<Tag> getAll();

    Tag getById(int tagId);


}
