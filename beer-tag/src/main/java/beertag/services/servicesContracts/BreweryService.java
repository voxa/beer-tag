package beertag.services.servicesContracts;

import beertag.models.Brewery;


public interface BreweryService {
    Brewery getById(int id);
    Brewery getByName(String name);

}
