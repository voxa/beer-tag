package beertag.services.servicesContracts;

import beertag.models.Style;

import java.util.List;

public interface StyleService {

  //  Style getById(int id);

    List<Style> getAll();


    Style getByName(String name);

}
