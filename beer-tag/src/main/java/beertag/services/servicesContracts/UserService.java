package beertag.services.servicesContracts;

import beertag.models.Beer;
import beertag.models.BeerRating;
import beertag.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

public interface UserService {


    User create(User user);

    void remove();

    User edit(User user);

    List<User> getAll();

    User getById(int id);


    User getByUsername(String username);

    List<Beer> getUserWishBeers();

    List<Beer> getUserDrankBeers();

  List<Beer> getTop3RankedBeers();

    List<Beer> getUserCreatedBeers();

    String markUnmarkAsWish(int beerId);

    String markUnmarkAsDrank( int beerId);

    String rateBeer( int beerId, int vote);

    int getUserRatingForBeer(int beerId);

    User getCurrentUser();

    void deleteUser(int userId);



}
