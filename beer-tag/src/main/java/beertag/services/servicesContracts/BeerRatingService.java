package beertag.services.servicesContracts;

import beertag.models.Beer;
import beertag.models.BeerRating;
import beertag.models.User;

import java.util.List;

public interface BeerRatingService {



   BeerRating create(BeerRating beerRating);

    BeerRating update(BeerRating beerRating);

    BeerRating getByIds(int userId, int beerId);



}
