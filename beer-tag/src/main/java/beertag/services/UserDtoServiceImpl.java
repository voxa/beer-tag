package beertag.services;

import beertag.models.User;
import beertag.DTOs.UserDto;
import beertag.repositories.repositoryContracts.UsersRepository;
import beertag.services.servicesContracts.UserDtoService;
import beertag.services.servicesContracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
//@RequiredArgsConstructor
public class UserDtoServiceImpl implements UserDtoService {

    private final UserService userService;
    private final BCryptPasswordEncoder bCryptEncoder;

    public UserDtoServiceImpl(UserService userService, BCryptPasswordEncoder bCryptEncoder) {
        this.userService = userService;
        this.bCryptEncoder = bCryptEncoder;
    }


    public User save(UserDto user) throws UsernameNotFoundException {
        User newUser = new User();
        newUser.setUsername(user.getUsername());
        newUser.setFirstName(user.getFirstName());
        newUser.setLastName(user.getLastName());
        newUser.setUserPicture(user.getUserPicture());
        newUser.setPassword(bCryptEncoder.encode(user.getPassword()));
        newUser.setEnabled(true);
        newUser.setRole("USER");

        return userService.create(newUser);
    }
}
