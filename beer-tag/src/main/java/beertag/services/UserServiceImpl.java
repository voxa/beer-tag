package beertag.services;

import beertag.models.Beer;
//import beertag.models.BeerRating;
import beertag.models.BeerRating;
import beertag.models.User;

import beertag.repositories.repositoryContracts.BeerRatingRepository;
import beertag.repositories.repositoryContracts.BeersRepository;
import beertag.repositories.repositoryContracts.UsersRepository;
import beertag.services.servicesContracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

import static beertag.Constants.*;

@Service
public class UserServiceImpl implements UserService {


    private UsersRepository userRepository;
    private BeersRepository beerRepository;
    private BeerRatingRepository beerRatingRepository;


    @Autowired
    public UserServiceImpl(UsersRepository userRepository, BeersRepository beerRepository, BeerRatingRepository beerRatingRepository) {
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
        this.beerRatingRepository = beerRatingRepository;
    }


    @Override
    public User create(User user) {

        User alreadyExist = userRepository.getByUsername(user.getUsername());

        if (alreadyExist !=null) {
            throw new IllegalArgumentException(
                    String.format(USER_WITH_NAME_ALREADY_EXISTS, user.getUsername()));
        }

        userRepository.create(user);
        return user;
    }

    @Override
    public void remove() {

        User user = getCurrentUser();

        if (!user.isEnabled()) {
            throw new IllegalArgumentException(
                    String.format(USER_WITH_ID_DO_NOT_EXISTS, user.getUserId()));
        }
        user.setEnabled(false);
        userRepository.update(user);
    }

    @Override
    public User edit(User newUser) { // къде

        User olduser = getCurrentUser();
        olduser.setUsername(newUser.getUsername());
        olduser.setFirstName(newUser.getFirstName());
        olduser.setLastName(newUser.getLastName());
        olduser.setUserPicture(newUser.getUserPicture());
        // olduser.setPassword(newUser.getPassword());

        // проверяване че сетнатите стойнсти отговарят на условията?
        return userRepository.update(olduser);
    } // ???

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int userId) {

        User user = userRepository.getById(userId);
        if (user == null) {
            throw new IllegalArgumentException(
                    String.format(USER_WITH_ID_DO_NOT_EXISTS, userId));
        }
        return user;
    }

    @Override
    public List<Beer> getUserWishBeers() {

        User user = getCurrentUser();

        if (user.getWishBeers().isEmpty()) {
            throw new IllegalArgumentException(
                    USER_DOES_NOT_HAVE_ANY_BEERS_IN_WISH_LIST);
        }
        return user.getWishBeers();
    }

    @Override
    public List<Beer> getUserDrankBeers() {

        User user = getCurrentUser();

        if (user.getDrankBeers().isEmpty()) {
            throw new IllegalArgumentException(
                    USER_DOES_NOT_HAVE_ANY_BEERS_IN_DRANK_LIST);
        }
        return user.getDrankBeers();
    }

    @Override
    public List<Beer> getTop3RankedBeers() {

        User user = getCurrentUser();
        int userId = user.getUserId();

        List<Beer> top3UserBeers = beerRatingRepository
                .getTop3UserRatedBeers(userId)
                .stream()
                .map(BeerRating::getBeer)
                .collect(Collectors.toList());

        if (top3UserBeers.isEmpty()) {
            throw new IllegalArgumentException(
                    USER_HAS_NOT_RATED_ANY_BEERS_YET);
        }
        return top3UserBeers;
    }


    @Override
    public List<Beer> getUserCreatedBeers() {

        User user = getCurrentUser();

        if (user.getCreatedBeers().isEmpty()) {
            throw new IllegalArgumentException(String.format(USER_HAS_NOT_CREATED_ANY_BEER_YET, user));
        }
        return user.getCreatedBeers();
    }


    @Override
    public String markUnmarkAsWish(int beerId) {

        User user = getCurrentUser();
        Beer beer = beerRepository.getById(beerId);

        List<Beer> userWishBeers = user.getWishBeers();

        for (Beer b : userWishBeers) {
            if (b.getBeerId() == beer.getBeerId()) {
                userWishBeers.remove(b);
                userRepository.update(user);
                return BEER_WAS_REMOVED_FROM_YOUR_WISH_LIST;
            }
        }

        user.getWishBeers().add(beer);
        userRepository.update(user);
        return BEER_WAS_ADDED_TO_YOUR_WISH_LIST;

//        if (user.getWishBeers().contains(beer)) {
//         user.getWishBeers().remove(beer);
//            userRepository.update(user);
//            return "Beer was removed from your wish list !";

    }


    @Override
    public String markUnmarkAsDrank(int beerId) {

        User user = getCurrentUser();
        Beer beer = beerRepository.getById(beerId);

        List<Beer> userDrankBeers = user.getDrankBeers();

        for (Beer b : userDrankBeers) {
            if (b.getBeerId() == beer.getBeerId()) {
                user.getDrankBeers().remove(b);
                userRepository.update(user);
                return BEER_WAS_REMOVED_FROM_YOUR_DRANK_LIST;
            }
        }

        List<Beer> userWishBeers = user.getWishBeers();

        for (Beer b : userWishBeers) {
            if (b.getBeerId() == beer.getBeerId()) {
                userWishBeers.remove(b);
                user.getDrankBeers().add(beer);
                userRepository.update(user);
                return CHEERS_BEER_WAS_MOVED_FROM_YOUR_WISH_LIST_TO_DRANK_LIST;
            }
        }

        user.getDrankBeers().add(beer);
        userRepository.update(user);
        return CHEERS_BEER_WAS_ADDED_TO_YOUR_DRANK_LIST;

    }

    @Override
    public String rateBeer(int beerId, int vote) {

        User user = getCurrentUser();
        Beer beer = beerRepository.getById(beerId);

        int userId = user.getUserId();
        double currAvgRating = beer.getAvgRating();
        int currVotes = beer.getVotes();

        double newAvgRating = 0;

        BeerRating brCheck = beerRatingRepository.getByIds(userId, beerId);

        if (brCheck == null) {

            List<Beer> userDrankBeers = user.getDrankBeers();

            int flag = 0;
            for (Beer b : userDrankBeers) {
                if (b.getBeerId() == beer.getBeerId()) {
                    flag = 1;
                }
            }

            if (flag != 1) {
                return THIS_BEER_IS_NOT_IN_YOUR_DRANK_LIST;
            }

            BeerRating br = new BeerRating(beer, user, vote);
            beerRatingRepository.create(br);
            newAvgRating = (currAvgRating * currVotes + vote) / (currVotes + 1);
            beer.setVotes(currVotes + 1);
            beer.setAvgRating(newAvgRating);
            beerRepository.update(beer);

        } else {

            int yourCurrentVote = getUserRatingForBeer(beerId);
            newAvgRating = (currAvgRating * currVotes - yourCurrentVote + vote) / (currVotes);
            beer.setAvgRating(newAvgRating);
            brCheck.setVote(vote);
            beerRatingRepository.update(brCheck);
            beerRepository.update(beer);
        }

        return THANK_YOU_FOR_YOUR_VOTE;

    }

    @Override
    public int getUserRatingForBeer(int beerId) {

        User user = getCurrentUser();
        int userId = user.getUserId();
        BeerRating br = beerRatingRepository.getByIds(userId, beerId);

        if (br == null) {
            throw new IllegalArgumentException(
                    USER_HAS_NOT_RATED_THIS_BEER_YET);
        }

        return br.getVote();
    }

    @Override
    public User getByUsername(String username) {
        User user = userRepository.getByUsername(username);
        if (user == null) {
            throw new IllegalArgumentException(
                    String.format(USER_WITH_NAME_DO_NOT_EXIST, username));
        }
        return user;

    }


    @Override
    public User getCurrentUser() {
        return userRepository.getCurrentUser();
    }

    @Override
    public void deleteUser(int userId) {

        User user = getById(userId);
        user.setEnabled(false);

        userRepository.update(user);


    }


}
