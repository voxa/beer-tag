package beertag.services;

import beertag.models.Tag;
import beertag.repositories.repositoryContracts.TagsRepository;
import beertag.services.servicesContracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private TagsRepository repository;

    @Autowired
    public TagServiceImpl(TagsRepository repository) {
        this.repository = repository;
    }


    @Override
    public void create(Tag tag) {
        String name = tag.getName();

        List<Tag> alreadyExist =
                repository
                        .getAll()
                        .stream()
                        .filter(u -> u.getName().equals(name))
                        .collect(Collectors.toList());

        if (!alreadyExist.isEmpty()) {
            throw new IllegalArgumentException(
                    String.format("Tag with name %s already exists", name));
        }
        repository.create(tag);

    }

    @Override
    public void remove(int id) {
        Tag tag = repository.getById(id);
        if (tag == null) {
            throw new IllegalArgumentException(
                    String.format("Tag with id %d does not exists!", id));
        }
        repository.getById( id);
    }

    @Override
    public Tag edit(Tag tag) {
        return  repository.update(tag);
    }

    @Override
    public List<Tag> getAll() {
        return repository.getAll();
    }

    @Override
    public Tag getById(int tagId) {
        Tag tag= repository.getById(tagId);
        if(tag== null){
            throw new IllegalArgumentException(String.format("Tag with %d doesn't exist",tagId));
        }
        return repository.getById(tagId);
    }


}
