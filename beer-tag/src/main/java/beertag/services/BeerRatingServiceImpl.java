package beertag.services;


import beertag.models.BeerRating;
import beertag.models.User;
import beertag.repositories.repositoryContracts.BeerRatingRepository;
import beertag.services.servicesContracts.BeerRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BeerRatingServiceImpl implements BeerRatingService {

      private BeerRatingRepository beerRatingRepository;


    @Autowired
    public BeerRatingServiceImpl(BeerRatingRepository beerRatingRepository) {

        this.beerRatingRepository = beerRatingRepository;
    }

    @Override
    public BeerRating create(BeerRating beerRating) {

        beerRatingRepository.create(beerRating);
        return beerRating;
    }


    @Override
    public BeerRating update(BeerRating beerRating) { // къде проверяване че сетнатите стойнсти отговарят на условията?
        return beerRatingRepository.update(beerRating);
    }


    @Override
    public BeerRating getByIds(int userId, int beerId) {

        BeerRating br = beerRatingRepository.getByIds(userId,beerId);
        if (br == null) {
            throw new IllegalArgumentException(
                  "N/A");
        }
        return br;
    }


}
