package beertag.repositories;

import beertag.models.Country;
import beertag.repositories.repositoryContracts.CountryRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@PropertySource(("classpath:application.properties"))
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Country> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country", Country.class);
        return query.list();
    }
    @Override
    public Country getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Country> query = session.createQuery("from Country where name = :name", Country.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }
}
