package beertag.repositories.repositoryContracts;

import beertag.models.User;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsersRepository  {

    User create(User user);

    User update(User user);

    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    User getCurrentUser();



}
