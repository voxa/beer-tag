package beertag.repositories.repositoryContracts;

import beertag.DTOs.BeerDTO;
import beertag.models.Beer;
import beertag.models.Country;
import beertag.models.Tag;

import java.util.List;
import java.util.Set;

public interface BeersRepository {



    Beer create(Beer Beer);

    String remove(int beerId);

    List<Beer> getAll();

    Beer getById(int beerId);

    Beer getByName(String name);

    Beer update(Beer beer);

// ~~~~~~

    List<Beer> getBeersByStyle(int styleId);

    List<Beer> getBeersByCountry(int countryId);

    Set<Beer> filterBeersByTag(List<String> tags);

    List<Beer> sortBeersByName();

    List<Beer> sortBeersByAbv();

    List<Beer> sortBeersByRating();

//    void addTag(Tag tag, int beerID);
//

}
