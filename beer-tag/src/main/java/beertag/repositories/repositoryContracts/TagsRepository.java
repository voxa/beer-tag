package beertag.repositories.repositoryContracts;

import beertag.models.Tag;
import beertag.models.User;

import java.util.List;

public interface TagsRepository {



    void create(Tag tag);

    void remove(int id);

    Tag update(Tag tag);

    List<Tag> getAll();

    Tag getById(int tagId);


}
