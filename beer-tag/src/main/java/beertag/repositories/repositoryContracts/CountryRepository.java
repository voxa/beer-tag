package beertag.repositories.repositoryContracts;

import beertag.models.Country;

import java.awt.*;
import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getByName(String name);

}
