package beertag.repositories.repositoryContracts;

import beertag.models.Beer;
import beertag.models.BeerRating;
import beertag.models.Tag;

import java.util.List;

public interface BeerRatingRepository {



    BeerRating create(BeerRating beerRating);


    BeerRating update(BeerRating beerRating);

    BeerRating getByIds(int userId, int beerId);

   List<BeerRating> getTop3UserRatedBeers(int userId);



}
