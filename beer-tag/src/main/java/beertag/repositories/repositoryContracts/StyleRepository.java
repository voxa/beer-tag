package beertag.repositories.repositoryContracts;

import beertag.models.Style;

import java.util.List;

public interface StyleRepository {



  //  Style getById (int id);

    //    @Override
    //    public Style getById(int id) {
    //        Style style;
    //
    //        try (Session session = sessionFactory.openSession()) {
    //            style = session.get(Style.class, id);
    //        } catch (HibernateException he) {
    //            System.out.println(he.getMessage());
    //            throw he;
    //        }
    //
    //        return style;
    //    }
    //
    Style getByName(String name);

    List<Style> getAll();

}
