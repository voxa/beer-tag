package beertag.repositories.repositoryContracts;

import beertag.models.Brewery;


public interface BreweryRepository {
    Brewery getById(int id);
    Brewery getByName(String name);

}
