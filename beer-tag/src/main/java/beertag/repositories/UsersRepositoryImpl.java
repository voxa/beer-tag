package beertag.repositories;


import beertag.models.User;
import beertag.repositories.repositoryContracts.UsersRepository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.context.annotation.PropertySource;


import java.util.List;


@Repository
@PropertySource("classpath:application.properties")

public class UsersRepositoryImpl implements UsersRepository {

    private SessionFactory sessionFactory;


    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }



    @Override
    public User create(User user) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(user);
        transaction.commit();
        return user;
    }


    @Override
    public List<User> getAll() {

        Session session = sessionFactory.openSession();
        Query<User> query = session.createQuery("from User", User.class);
        return query.list();
    }

    @Override
    public User update(User user) {

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(user);
        transaction.commit();
        return user;
    }

    @Override
    public User getById(int userId) {

        Session session = sessionFactory.openSession();

        Query<User> query = session.createQuery(" from User where userId=:userId", User.class);
        query.setParameter("userId", userId);
        query.getSingleResult();

        return  query.getSingleResult();
    }

    @Override
    public User getByUsername(String username) {



        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User as u where username = :username and u.isEnabled=true ", User.class);
            query.setParameter("username", username);

            return query.uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

    }

    public User getCurrentUser() {

        Authentication authentication = SecurityContextHolder.getContext().
                getAuthentication();
        return getByUsername(authentication.getName());
    }



}





