package beertag.repositories;


import beertag.models.*;

import beertag.repositories.repositoryContracts.BeersRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static beertag.Constants.*;


@Repository
@PropertySource("classpath:application.properties")
public class BeersRepositoryImpl implements BeersRepository {



    private SessionFactory sessionFactory;

    @Autowired
    public BeersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Beer create(Beer beer) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(beer);
        transaction.commit();
        return beer;
    }

    @Override
    public String remove(int beerId) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Beer beerToRemove = getById(beerId); //session.get(Beer.class, beerId);
        session.delete(beerToRemove);
        transaction.commit();

        return "Successfully removed!";

//        try (Session session = sessionFactory.openSession()) {
//            Transaction tx = session.beginTransaction();
//            Beer beer = session.get(Beer.class, beerId);
//
//            if (beer != null) session.delete(beer);
//            else {
//                tx.rollback();
//                throw new ResponseStatusException(HttpStatus.CONFLICT, BEER_DOES_NOT_EXISTS);
//            }
//            tx.commit();
//            return SUCCESSFULLY_DELETED;
//        } catch (HibernateException he) {
//            throw he;
//        }
    }

    @Override
    public List<Beer> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer", Beer.class);
        return query.list();
    }


    @Override
    public Beer getByName(String name) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name = :name", Beer.class);
            query.setParameter("name", name);
            if (query.list().isEmpty()) {
                return null;
            } else return query.uniqueResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }
    @Override
    public Beer update(Beer beer) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.update("beer", beer);
        transaction.commit();

        return session.get(Beer.class, beer.getBeerId());
    }

    @Override
    public Beer getById(int id) {

        Session session = sessionFactory.getCurrentSession();
        return session.get(Beer.class, id);


    }

    @Override
    public List<Beer> sortBeersByName() {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer order by name", Beer.class);
        return query.list();
    }

    @Override
    public List<Beer> sortBeersByAbv() {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer " +
                "order by abv,avgRating,votes,name desc", Beer.class);
        return query.list();
    }

    @Override
    public List<Beer> sortBeersByRating() {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer" +
                " order by avgRating,votes,abv,name desc", Beer.class);
        return query.list();
    }


    @Override
    public List<Beer> getBeersByStyle(int styleId) {
        Session session = sessionFactory.getCurrentSession();
        Style style = session.get(Style.class, styleId);
        Query<Beer> query = session.createQuery("from Beer where style = :id " +
                "order by  avgRating,votes,abv,name desc", Beer.class);
        query.setParameter("id", style);
        return query.list();
    }

    @Override
    public List<Beer> getBeersByCountry(int countryId) {
        Session session = sessionFactory.getCurrentSession();
        Country country = session.get(Country.class, countryId);
        Query<Beer> query = session.createQuery("from Beer where country = :id " +
                "order by  avgRating,votes,abv,name desc", Beer.class);
        query.setParameter("id", country);
        return query.list();
    }


    @Override
    public Set<Beer> filterBeersByTag(List<String> tags) {
        Session session = sessionFactory.getCurrentSession();
        Set<Beer> filteredByTags = new HashSet<>();
        Query<Beer> query = session.createQuery("select b from Beer  b join b.tags t where t.name in (:tags) group by t.name ", Beer.class);
        query.setParameterList("tags", tags);
        filteredByTags.addAll(query.list());
        return filteredByTags;

    }
}

