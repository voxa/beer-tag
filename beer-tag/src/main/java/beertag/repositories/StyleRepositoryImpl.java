package beertag.repositories;

import beertag.models.Style;
import beertag.repositories.repositoryContracts.StyleRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@PropertySource(("classpath:application.properties"))
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    //    @Override
//    public Style getById(int id) {
//        Style style;
//
//        try (Session session = sessionFactory.openSession()) {
//            style = session.get(Style.class, id);
//        } catch (HibernateException he) {
//            System.out.println(he.getMessage());
//            throw he;
//        }
//
//        return style;
//    }
//
    @Override
    public Style getByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style where name = :name", Style.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }



    @Override
    public List<Style> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Style> query = session.createQuery("from Style", Style.class);
        return query.list();
    }
}
