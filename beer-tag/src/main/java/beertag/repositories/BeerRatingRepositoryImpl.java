package beertag.repositories;

import beertag.models.BeerRating;
import beertag.repositories.repositoryContracts.BeerRatingRepository;

import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;


import java.util.List;


@Repository
@PropertySource("classpath:application.properties")
public class BeerRatingRepositoryImpl implements BeerRatingRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public BeerRatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public BeerRating create(BeerRating beerRating) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(beerRating);
        session.getTransaction().commit();

        return  beerRating;
    }

    @Override
    public BeerRating update(BeerRating beerRating) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.update(beerRating);
        session.getTransaction().commit();

        return beerRating;

    }


    // BASE 64 CLOB

    @Override
    public BeerRating getByIds(int userId, int beerId) {

   BeerRating userVote =null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            Query<BeerRating> query = session.createQuery("from BeerRating as br" +
                    " where br.user.id=:userId and br.beer.id=:beerId ", BeerRating.class)
           .setParameter("userId", userId)
                    .setParameter("beerId", beerId);
            userVote=query.setMaxResults(1).uniqueResult();

            transaction.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return userVote;

    }

    @Override
   public List<BeerRating> getTop3UserRatedBeers(int userId){

        Session session = sessionFactory.getCurrentSession();

        Query<BeerRating> top3 = session.createQuery("from BeerRating as br" +

                " where br.user.id=:userId   order by br.vote , br.beer.avgRating   ", BeerRating.class)
                .setParameter("userId", userId)
                .setMaxResults(3);

        return top3.list();

    }



}
