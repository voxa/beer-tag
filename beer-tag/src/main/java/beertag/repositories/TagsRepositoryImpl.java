package beertag.repositories;


import beertag.models.Tag;
import beertag.repositories.repositoryContracts.TagsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@PropertySource("classpath:application.properties")
public class TagsRepositoryImpl implements TagsRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public TagsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Tag tag) {
        Session session=sessionFactory.getCurrentSession();
        session.save(tag);
    }

    @Override
    public void remove(int id) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Tag tagToRemove = session.get(Tag.class, id);
        session.delete(tagToRemove);
        transaction.commit();
    }

    @Override
    public Tag update(Tag tag) {
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        session.update("tag", tag);
        transaction.commit();

        return session.get(Tag.class, tag.getTagId());

    }

    @Override
    public List<Tag> getAll() {
        Session session = sessionFactory.getCurrentSession();
        Query<Tag> query = session.createQuery("from Tag", Tag.class);
        return query.list();
    }

    @Override
    public Tag getById(int tagId) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Tag.class, tagId);
    }


}
