package beertag.repositories;

import beertag.models.Brewery;
import beertag.models.Style;
import beertag.repositories.repositoryContracts.BreweryRepository;
import beertag.repositories.repositoryContracts.StyleRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource("classpath:application.properties")
public class BreweryRepositoryImpl implements BreweryRepository {
    private SessionFactory sessionFactory;


    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public Brewery getById(int id) {
        Brewery brewery;
        try (Session session = sessionFactory.openSession()) {
            brewery = session.get(Brewery.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

        return brewery;
    }

    @Override
    public Brewery getByName(String name) {
        Brewery brewery;
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery" +
                    " where name = :name", Brewery.class);
            query.setParameter("name", name);

            brewery = query.getSingleResult();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        return brewery;
    }
}
