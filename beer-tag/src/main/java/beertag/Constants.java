package beertag;

public class Constants {


    public static final int UserName_Min_Length = 3;
    public static final int UserName_Max_Length = 12;
    public static final String UserName_ERROR_Message = " User name cannot be less than 3 or more than 12 symbols long! ";

    public static final int UserPassword_Min_Length = 4;
    public static final int UserPassword_Max_Length = 10;
    public static final String UserPassword_ERROR_Message = " User password cannot be less than 4 or more than 10 symbols long! ";

    public static final int BeerName_Min_Length = 4;
    public static final int BeerName_Max_Length = 20;
    public static final String BeerName_ERROR_Message = " Beer name cannot be less than 4 or more than 20d symbols long! ";

    public static final int BreweryName_Min_Length = 5;
    public static final int BreweryName_Max_Length = 40;
    public static final String BreweryName_ERROR_Message = " Brewery name cannot be less than 5 or more than 40 symbols long! ";

    public static final int TagName_Min_Length = 3;
    public static final int TagName_Max_Length = 40;
    public static final String TagName_ERROR_Message = " Tag name cannot be less than 3 or more than 40 symbols long! ";

    //UserService
    public static final String USER_WITH_NAME_ALREADY_EXISTS = "User with name %s already exists";
    public static final String USER_WITH_ID_DO_NOT_EXISTS = "User with id %d do not exists!";
    public static final String USER_DOES_NOT_HAVE_ANY_BEERS_IN_WISH_LIST = "User does not have any beers in wish list!";
    public static final String USER_DOES_NOT_HAVE_ANY_BEERS_IN_DRANK_LIST = "User does not have any beers in drank list!";
    public static final String USER_HAS_NOT_RATED_ANY_BEERS_YET = "User has not rated any beers yet !";
    public static final String USER_HAS_NOT_CREATED_ANY_BEER_YET = "%s has not created  any beer yet!";
    public static final String BEER_WAS_REMOVED_FROM_YOUR_WISH_LIST = "Beer was removed from your wish list !";
    public static final String BEER_WAS_ADDED_TO_YOUR_WISH_LIST = "Beer was added to your wish list !";
    public static final String BEER_WAS_REMOVED_FROM_YOUR_DRANK_LIST = "Beer was removed from your drank list !";
    public static final String CHEERS_BEER_WAS_MOVED_FROM_YOUR_WISH_LIST_TO_DRANK_LIST = " CHEERS! Beer was moved from your wish list to drank list!";
    public static final String CHEERS_BEER_WAS_ADDED_TO_YOUR_DRANK_LIST = "CHEERS! Beer was added to your drank list !";
    public static final String THIS_BEER_IS_NOT_IN_YOUR_DRANK_LIST = " This beer is not in your drank list! ";
    public static final String THANK_YOU_FOR_YOUR_VOTE = "Thank you for your vote!";
    public static final String USER_HAS_NOT_RATED_THIS_BEER_YET = "User has not rated this beer yet!";
    public static final String USER_WITH_NAME_DO_NOT_EXIST = "User with name %s do not exist !";

    //BeerService
    public static final String BEER_DOES_NOT_EXISTS = "Beer does not exists";
    public static final String SUCCESSFULLY_DELETED = "Successfully deleted";
    public static final String YOU_HAVE_TO_REGISTER_FIRST = "You have to register first!";
    public static final String TAG_WITH_NAME_ALREADY_EXISTS = "Tag with name %s already exists";
    public static final String TAG_CREATED_SUCCESSFULLY = "Tag created successfully";
    public static final String BEER_WITH_NAME_ALREADY_EXISTS = "Beer with name %s already exists";
    public static final String BEER_WITH_ID_DOESN_T_EXIST = "Beer with id %d doesn't exist";
    public static final String SUCCESSFULLY_REMOVED = "Successfully removed";

    // Beer Model
    public static final String BEER_ID = "beerId";
    public static final String NAME = "name";
    public static final String BREWERY_ID = "brewery_id";
    public static final String COUNTRY_ID = "country_id";
    public static final String ABV = "abv";
    public static final String AVG_RATING = "avgRating";
    public static final String VOTES = "votes";
    public static final String STYLE_ID = "style_id";
    public static final String DESCRIPTION = "description";
    public static final String CREATOR_ID = "creator_id";
    public static final String PICTURE = "picture";
    public static final String BEER = "beer";
    public static final String BEERS_TAGS = "beers_tags";
    public static final String TAG_ID = "tagId";

    //User Model
    public static final String USER_ID = "userId";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String ROLE = "role";
    public static final String IS_ENABLED = "isEnabled";
    public static final String WISH_LIST = "wish_list";
    public static final String DRANK_LIST = "drank_list";
    public static final String USER = "user";
    public static final String CREATED_BY_USER = "created_by_user";

    //Tags
    public static final String ID = "id";
    public static final String TAGS = "tags";

    //Country
    public static final String CODE = "code";
    public static final String COUNTRY = "country";

    //BeerRating
    public static final String VOTE = "vote";
    public static final String RATING_ID = "rating_id";
}
