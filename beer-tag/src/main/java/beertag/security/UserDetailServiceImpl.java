package beertag.security;

import beertag.models.User;
import beertag.repositories.repositoryContracts.UsersRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public  class UserDetailServiceImpl implements UserDetailsService {

    private final UsersRepository repository;

    public UserDetailServiceImpl(UsersRepository repository) {
        this.repository = repository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User currentUser = repository.getByUsername(username);

     //    int userId = currentUser.getUserId();
   //    System.out.println(1+currentUser.getUsername());
        return new org.springframework.security.core.userdetails.User(username, currentUser.getPassword()
                , true, true, true,
                true, getAuthorities(currentUser));
    }

    private Set<SimpleGrantedAuthority> getAuthorities(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));

        return authorities;
    }
}