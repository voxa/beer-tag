package beertag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;

@SpringBootApplication(scanBasePackages = { "beertag" }, exclude = { ErrorMvcAutoConfiguration.class })
public class BeerTagApplication {

    public static void main(String[] args) {


        SpringApplication.run(BeerTagApplication.class, args);


    }

}
