package beertag.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;


@Configuration
@EnableSwagger2
@EnableAutoConfiguration
public class SwaggerConfig {


    @Bean
    public Docket apiDocket() {

//        return new Docket(DocumentationType.SWAGGER_2)
//                .select()
//              .apis(RequestHandlerSelectors.any())
//            //    .apis(RequestHandlerSelectors.basePackage("beertag"))
//             //   .paths(PathSelectors.any())
//                .paths(regex("/api.*"))
//                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metaInfo());

    }


    private ApiInfo metaInfo() {

        List<VendorExtension> vendorExtensions = new ArrayList<>();

        ApiInfo apiInfo = new ApiInfo("BeerTag",
                "BeerTag API",
                "1.0",
                "Terms of service",
        new Contact("VNS",
                "https://www.youtube.com/watch?v=kl8mpAvTm_Y",
               " voxa@mail.bg" ),
                "",
                "",
                vendorExtensions
        );

        return apiInfo;
    }


    //   public ApiInfo(
    //      String title,
    //      String description,
    //      String version,
    //      String termsOfServiceUrl,
    //      Contact contact,
    //      String license,
    //      String licenseUrl,
    //      Collection<VendorExtension> vendorExtensions) {
    //    this.title = title;
    //    this.description = description;
    //    this.version = version;
    //    this.termsOfServiceUrl = termsOfServiceUrl;
    //    this.contact = contact;
    //    this.license = license;
    //    this.licenseUrl = licenseUrl;
    //    this.vendorExtensions = newArrayList(vendorExtensions);
    //  }





}
