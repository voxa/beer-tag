package beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.List;
import java.util.Set;

import static beertag.Constants.*;

@Entity
@Table(name = "users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = USER_ID)
    private int userId;

    @NotNull
    @Size(min = UserName_Min_Length,
            max = UserName_Max_Length,
            message = UserName_ERROR_Message)
    @Column(name = USERNAME)
    private String username;

    @NotNull
    @Column(name = PASSWORD)
    private String password;

    @NotNull
    @Column(name = FIRST_NAME)
    private String firstName;

    @NotNull
    @Column(name = LAST_NAME)
    private String lastName;

    @NotNull
    @Column(name = PICTURE)
    private String userPicture;

    @Column(name = ROLE)
    private String role;

    @Column(name = IS_ENABLED)
    private boolean isEnabled;

   // @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany(fetch = FetchType.EAGER )
    @JoinTable(name = WISH_LIST,
            joinColumns = @JoinColumn(name = USER_ID),
            inverseJoinColumns = @JoinColumn(name = BEER_ID))
    @JsonIgnore
    private List<Beer> wishBeers;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(name = DRANK_LIST,
            joinColumns = @JoinColumn(name = USER_ID),
            inverseJoinColumns = @JoinColumn(name = BEER_ID))
    @JsonIgnore
    private List<Beer> drankBeers;


    @OneToMany(mappedBy = USER) //,  fetch = FetchType.EAGER)
    @JsonIgnore
    private List<BeerRating> ratedBeers;


   @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany
    @JoinTable(name = CREATED_BY_USER,
            joinColumns = @JoinColumn(name = USER_ID),
            inverseJoinColumns = @JoinColumn(name = BEER_ID))
   @JsonIgnore
    private List<Beer> createdBeers;


    public User() {
    }

    public User(String username, String password, String firstName, String lastName, String userPicture) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userPicture = userPicture;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(String userPicture) {
        this.userPicture = userPicture;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    public List<Beer> getWishBeers() {
        return wishBeers;
    }

    public void setWishBeers(List<Beer> wishBeers) {
        this.wishBeers = wishBeers;
    }

    public List<Beer> getDrankBeers() {
        return drankBeers;
    }

    public void setDrankBeers(List<Beer> drankBeers) {
        this.drankBeers = drankBeers;
    }

    public List<BeerRating> getRatedBeers() {
        return ratedBeers;
    }

    public void setRatedBeers(List<BeerRating> ratedBeers) {
        this.ratedBeers = ratedBeers;
    }

    public List<Beer> getCreatedBeers() {
        return createdBeers;
    }

    public void setCreatedBeers(List<Beer> createdBeers) {
        this.createdBeers = createdBeers;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        this.isEnabled = enabled;
    }
}
