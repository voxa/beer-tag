package beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static beertag.Constants.ID;
import static beertag.Constants.NAME;

@Entity
@Table(name = "beer_style")
public class Style {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private int id;

    @NotNull
    @Column(name = NAME)
        private String name;

    public Style() {
    }

    public Style(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
