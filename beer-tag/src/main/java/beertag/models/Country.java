package beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import static beertag.Constants.*;

@Entity
@Table(name = "countries")
public class Country {



    @Id
       @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private int id;

    @NotNull
    @Column(name = CODE)
    private String code;

    @NotNull
    @Column(name = NAME)
       private String name;

    @OneToMany(mappedBy = COUNTRY)
    @JsonIgnore
    private List<Beer> beers;

    public Country() {
    }

    public Country( String code,String name) {
        this.code=code;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Beer> getBeers() {
        return beers;
    }

    public void setBeers(List<Beer> beers) {
        this.beers = beers;
    }
}
