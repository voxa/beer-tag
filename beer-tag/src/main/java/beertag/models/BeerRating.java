package beertag.models;

import javax.persistence.*;

import static beertag.Constants.*;

@Entity
@Table(name = "rating")
public class BeerRating {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = RATING_ID)
    private int ratingId;

    @ManyToOne
    @JoinColumn(name = BEER_ID)
    private Beer beer;

    @ManyToOne
    @JoinColumn(name = USER_ID)
    private User user;

    @Column(name = VOTE)
    private int vote;

    public BeerRating() {

    }

    public BeerRating(Beer beer, User user, int vote) {
        this.beer = beer;
        this.user = user;
        this.vote = vote;
    }


    public int getRatingId() {
        return ratingId;
    }

    public void setRatingId(int ratingId) {
        this.ratingId = ratingId;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }


}

