package beertag.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static beertag.Constants.*;

@Entity
@Table(name = "breweries")
public class Brewery {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private int id;

    @Column(name = NAME)
    @Size(min = BreweryName_Min_Length,
            max = BreweryName_Max_Length,
            message = BreweryName_ERROR_Message)
    @NotNull
       private String name;

    public Brewery( ) {

    }


    public Brewery( String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
