package beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import static beertag.Constants.*;

@Entity
@Table(name = "beers")
public class Beer {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = BEER_ID)
    private int beerId;



    @Size(min = BeerName_Min_Length,
            max = BeerName_Max_Length,
            message = BeerName_ERROR_Message)

    @Column(name = NAME)
    @NotNull
    private String name;

    @ManyToOne
    @JoinColumn(name = BREWERY_ID)
    private Brewery brewery;

    @ManyToOne
    @NotNull
    @JoinColumn(name = COUNTRY_ID)
    private Country country;


    @NotNull
    @Column(name = ABV)
    private Double abv;

    @Column(name = AVG_RATING)
    private Double avgRating;

    @Column(name = VOTES)
    private Integer votes;

    @NotNull
    @ManyToOne
    @JoinColumn(name = STYLE_ID)
    private Style style;


    @Column(name = DESCRIPTION)
    private String description;


    @ManyToOne
    @JoinColumn(name = CREATOR_ID)
    // @JsonIgnore
    private User creator;


    @Column(name = PICTURE)
    private String beerPicture;

    @OneToMany(mappedBy = BEER)
    @JsonIgnore
    private List<BeerRating> ratedBeers;

    @ManyToMany //(cascade = CascadeType.ALL)
    @JoinTable(name = BEERS_TAGS,
            joinColumns = @JoinColumn(name = BEER_ID),
            inverseJoinColumns = @JoinColumn(name = TAG_ID))
    @JsonIgnore
    private List<Tag> tags = new ArrayList<>();


    public Beer() {

    }

    public Beer(String name, Country country, Double abv, Style style, User creator , String beerPicture) {

        this.name = name;
        this.country = country;
        this.abv = abv;
        this.style = style;
        this.creator = creator;
        this.beerPicture= beerPicture;

    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Double getAbv() {
        return abv;
    }

    public void setAbv(Double abv) {
        this.abv = abv;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }

    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public String getBeerPicture() {
        return beerPicture;
    }

    public void setBeerPicture(String beerPicture) {
        this.beerPicture = beerPicture;
    }

    public List<BeerRating> getRatedBeers() {
        return ratedBeers;
    }

    public void setRatedBeers(List<BeerRating> ratedBeers) {
        this.ratedBeers = ratedBeers;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag tag) {
        this.tags.add(tag);
    }




}