package beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.HashSet;
import java.util.Set;

import static beertag.Constants.*;

@Entity
@Table(name="tags")
public class Tag {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID)
    private int tagId;

    @Column(name = NAME)
    @Size(min = TagName_Min_Length,
            max = TagName_Max_Length,
            message = TagName_ERROR_Message)
    @NotNull
    private String name;

    @ManyToMany(mappedBy = TAGS,fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Beer> beers;


    public Tag() {
    }



    public Tag( String name) {

        this.name = name;

    }


    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Beer> getBeers() {
        return beers;
    }

    public void setBeers(Set<Beer> tagsOnBeers) {
        this.beers = tagsOnBeers;
    }
}
